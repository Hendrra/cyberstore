from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, viewsets

from ...models import Product
from ..filters import ProductFilter
from ..serializers import ProductSerializer


class ProductList(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_class = ProductFilter
    search_fields = ["name", "description"]
