import django_filters
from django.db.models import Q
from ...models import Product


class ProductFilter(django_filters.FilterSet):
    tags = django_filters.CharFilter(method="filter_tags")

    class Meta:
        model = Product
        fields = {
            "price": ["exact", "lte", "gte"],
            "quantity": ["exact", "lte", "gte"],
            "name": ["icontains"],
            "description": ["icontains"],
        }

    def filter_tags(self, queryset, name, value):
        return queryset.filter(Q(tags__tags__icontains=value))
