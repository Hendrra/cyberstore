from django.core.validators import MinValueValidator
from django.db import models


# Create your models here.
class Product(models.Model):
    # basic information
    name = models.CharField(max_length=32, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=False, null=False)
    price = models.FloatField(validators=[MinValueValidator(0.0)], blank=False, null=False)
    quantity = models.IntegerField(validators=[MinValueValidator(0)], blank=False, null=False)

    # url to the picture
    image = models.CharField(max_length=255, blank=True, null=True)

    # other information
    tags = models.JSONField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}, {self.price}$"

    def __repr__(self):
        return f"{self.name}, {self.price}$"
