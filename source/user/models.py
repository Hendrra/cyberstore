from django.db import models

TITLES = [
    ("", ""),  # no title
    ("Mr", "Mr"),  # any man
    ("Mrs", "Mrs"),  # married woman who uses her husband's last name
    ("Ms", "Ms"),  # married or unmarried woman; common in business
    ("Miss", "Miss"),  # unmarried woman
]


# Create your models here.
class User(models.Model):
    # personal information
    firstname = models.CharField(max_length=64, blank=False, null=False)
    lastname = models.CharField(max_length=64, blank=False, null=False)
    title = models.CharField(max_length=4, choices=TITLES, blank=False, null=False)

    # account details
    email = models.EmailField(max_length=64, blank=False, null=False)
    password = models.CharField(max_length=128, blank=False, null=False)

    # verification information
    token = models.UUIDField(null=False)
    verified = models.BooleanField(default=False, blank=False, null=False)

    # address information
    address = models.CharField(max_length=128, blank=False, null=False)
    city = models.CharField(max_length=64, blank=False, null=False)
    zip = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return f"{self.firstname} {self.lastname}: {self.zip}"

    def __repr__(self):
        return f"{self.firstname} {self.lastname}: {self.zip}"
